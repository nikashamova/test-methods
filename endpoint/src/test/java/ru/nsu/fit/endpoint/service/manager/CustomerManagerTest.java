package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());

        when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithNullArgument() {
        customerManager.createCustomer(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCustomerWithEasyPassword() {
        customerBeforeCreateMethod.setPass("123qwe");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFirstNameNotCapitalFirst() {
        customerBeforeCreateMethod.setFirstName("samplename");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLastNameNotCapitalFirst() {
        customerBeforeCreateMethod.setLastName("samplename");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFirstNameIsShort() {
        customerBeforeCreateMethod.setFirstName("s");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLastNameIsShort() {
        customerBeforeCreateMethod.setLastName("s");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFirstNameIsLong() {
        customerBeforeCreateMethod.setFirstName("1234567890qwe");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLastNameIsLong() {
        customerBeforeCreateMethod.setLastName("1234567890qwe");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFirstNameContainsUpperCase() {
        customerBeforeCreateMethod.setFirstName("QweQw");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFirstNameContainsNumber() {
        customerBeforeCreateMethod.setFirstName("Qwe1w");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testLastNameContainsNumber() {
        customerBeforeCreateMethod.setLastName("Qwe1w");
        customerManager.createCustomer(customerBeforeCreateMethod);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateBalance() {
        when(dbService.getCustomerById(anyString())).thenReturn(customerBeforeCreateMethod);
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
        customer.setBalance(25);
        customerManager.updateCustomer(customer);
    }

    @Test
    public void testUpdateOk() {
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
        when(dbService.getCustomerById(anyString())).thenReturn(customer);
        customer.setFirstName("Aaa");
        customer.setLastName("Aaa");
        customerManager.updateCustomer(customer);
        Assert.assertEquals(3, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testFirstNameIsOkShort() {
        customerBeforeCreateMethod.setFirstName("Aa");
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testFirstNameIsOkLong() {
        customerBeforeCreateMethod.setFirstName("Aaaaaaaaaaaa");
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().size());
    }

}
