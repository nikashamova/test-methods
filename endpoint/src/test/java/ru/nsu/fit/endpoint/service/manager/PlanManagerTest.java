package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

public class PlanManagerTest {

    private DBService dbService;
    private Logger logger;
    private PlanManager planManager;

    private Plan planeBefore;

    @Before
    public void before() {
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);
        planManager = new PlanManager(dbService, logger);
        planeBefore = new Plan();
        planeBefore.setDetails("There are details");
        planeBefore.setFee(100);
        planeBefore.setName("fee");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShortName() {
        planeBefore.setName("a");
        planManager.createPlan(planeBefore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLongName() {
        planeBefore.setName(RandomStringUtils.random(129));
        planManager.createPlan(planeBefore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSmallFee() {
        planeBefore.setFee(-1);
        planManager.createPlan(planeBefore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLargeFee() {
        planeBefore.setFee(999999 + 1);
        planManager.createPlan(planeBefore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShortDescription() {
        planeBefore.setDetails("");
        planManager.createPlan(planeBefore);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLongDescription() {
        planeBefore.setDetails(RandomStringUtils.random(1025));
        planManager.createPlan(planeBefore);
    }


}
