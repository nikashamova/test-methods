package ru.nsu.fit.services.browser;

import com.google.common.base.Predicate;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import ru.nsu.fit.tests.gui.screen.CustomersScreen;
import ru.nsu.fit.tests.gui.screen.LoginScreen;

import java.io.Closeable;
import java.util.concurrent.TimeUnit;

public class BrowserService implements Closeable {
    private static final int waitTimeout = 10;
    private static final String customersUrl = "http://localhost:8080/endpoint/customers.html";
    private static final String loginUrl = "http://localhost:8080/endpoint/login.html";

    private WebDriver webDriver;

    public BrowserService() {
        try {
            System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
            this.webDriver = new ChromeDriver();
            this.webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void sendKeys(WebElement element, String value) {
        element.sendKeys(value);
        makeScreenshot();
        Reporter.log(String.format("Send key '%s' to element '%s'\n", value, element));
    }

    public void click(WebElement element) {
        Reporter.log(String.format("Click element '%s'", element));
        element.click();
    }

    public void waitElement(WebElement element) {
        new WebDriverWait(webDriver, waitTimeout).until(ExpectedConditions.visibilityOf(element));
        Reporter.log(String.format("Element '%s' is ready\n", element));
    }

    public LoginScreen openLoginPage() {
        webDriver.get(loginUrl);
        Reporter.log(String.format("Open '%s'\n", loginUrl));
        return new LoginScreen(this);
    }

    public CustomersScreen openCustomersPage() {
        webDriver.get(customersUrl);
        Reporter.log(String.format("Open '%s'\n", customersUrl));
        return new CustomersScreen(this);
    }

    public void makeScreenshot() {
        try {
            String screenshot = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BASE64);
            Reporter.log(String.format("Screenshot:\n<img src=\"data:image.png;base64, %s \"/>", screenshot));
        } catch (UnhandledAlertException e){
            Reporter.log("Alert window is presented");
        }
    }

    public boolean isElementPresent(By element) {
        boolean elementPresent = webDriver.findElements(element).size() > 0;
        Reporter.log(String.format("Element %s present=%s", element, elementPresent));
        return elementPresent;
    }

    public void waitFor(int seconds) {
        Reporter.log(String.format("Waiting %d seconds\n", seconds));
        WebDriverWait wait = new WebDriverWait(webDriver, seconds);
        try {
            wait.until((Predicate<WebDriver>) driver -> false);
        } catch (TimeoutException ignored) {
        }
    }

    public boolean isAlertPresent() {
        boolean alertPresent;
        try {
            webDriver.switchTo().alert();
            alertPresent = true;
        } catch (NoAlertPresentException Ex) {
            alertPresent = false;
        }
        Reporter.log(String.format("Alert present=%s", alertPresent));
        return alertPresent;
    }

    public void acceptAlert() {
        Alert alert = webDriver.switchTo().alert();
        alert.accept();
        Reporter.log("Alert accepted\n");
    }


    public String getFirstRowText(int column) {
        String text = webDriver.findElement(By.xpath(String.format("(//table//td)[%d]\n", column))).getText();
        makeScreenshot();
        return text;
    }

    @Override
    public void close() {
        webDriver.close();
    }
}
