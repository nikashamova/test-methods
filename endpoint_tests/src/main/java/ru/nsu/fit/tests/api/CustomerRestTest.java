package ru.nsu.fit.tests.api;

import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.tests.entity.Role;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class CustomerRestTest {
    private final TestService testService = new TestService();
    private String randomSuffix;

    @BeforeClass
    public void before() {
        randomSuffix = RandomStringUtils.randomAlphabetic(3);
    }

    @Test(description = "Create customer")
    public void testCreateCustomer() {
        Customer customer = new Customer("Илон", "Маск", "boring" + randomSuffix, "spacex", Integer.MAX_VALUE);
        Response response = testService.post("rest/create_customer", customer);
        Assert.assertEquals(response.getStatus(), 200);
        Customer responseCustomer = response.readEntity(Customer.class);
        Assert.assertNotNull(responseCustomer.getId());
    }

    @Test(description = "Get customer by login", dependsOnMethods = "testCreateCustomer")
    public void testGetCustomer() {
        Response response = testService.get("rest/get_customer_id/" + "boring" + randomSuffix, null, true);
        Customer responseCustomer = response.readEntity(Customer.class);
        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertEquals(responseCustomer.getLogin(), "boring" + randomSuffix);
    }

    @Test(description = "Get customer by incorrect login")
    public void testGetCustomerFail() {
        Response response = testService.get("rest/get_customer_id/" + randomSuffix, null, true);
        Assert.assertEquals(response.getStatus(), 400);
    }

    @Test(description = "Get customer list", dependsOnMethods = "testCreateCustomer")
    public void testGetCustomers() {
        Response response = testService.get("rest/get_customers", null, true);
        Customer[] customers = response.readEntity(Customer[].class);
        Assert.assertTrue(
                Stream.of(customers)
                        .anyMatch((customer -> ("boring" + randomSuffix).equals(customer.getLogin()))));
        Assert.assertEquals(response.getStatus(), 200);
    }

    @Test(description = "Server health check")
    public void testHealthCheck() {
        Response response = testService.get("rest/health_check", null, false);
        Assert.assertEquals(response.getStatus(), 200);
    }

    @Test(description = "Test admin role")
    public void testAdminRole() {
        Response response = testService.get("rest/get_role", null, true);
        Role role = response.readEntity(Role.class);
        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertEquals(role.getRole(), "ADMIN");
    }

    @Test(description = "Test unknown role")
    public void testUnknownRole() {
        Response response = testService.get("rest/get_role", null, false);
        Role role = response.readEntity(Role.class);
        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertEquals(role.getRole(), "UNKNOWN");
    }

    private Map<String, Object> createParameters(String[] keys, Object[] values) {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>(keys.length);
        for (int i = 0; i < keys.length; i++) {
            stringObjectHashMap.put(keys[i], values[i]);
        }
        return stringObjectHashMap;
    }
}
