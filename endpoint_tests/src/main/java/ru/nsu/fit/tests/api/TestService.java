package ru.nsu.fit.tests.api;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

public class TestService {

    private static final String endpoint = "http://localhost:8080/endpoint";

    public Response post(final String path, final Object body) {
        Response response = getBuilder(path, null, true).post(Entity.entity(body, MediaType.APPLICATION_JSON));
        response.bufferEntity();

        Logger.debug("\n[POST] " + path + "\nREQUEST BODY: " + body + "\nRESPONSE: " + response.readEntity(String.class));
        return response;
    }

    public Response get(final String path, final Map<String, Object> queryParams, final boolean login) {

        Response response = getBuilder(path, queryParams, login).get();
        response.bufferEntity();

        Logger.debug("\n[GET] " + path + "\nRESPONSE: " + response.readEntity(String.class));
        return response;
    }


    private Invocation.Builder getBuilder(final String path, final Map<String, Object> queryParams, final boolean loginAsAdmin) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(JacksonFeature.class);
        if (loginAsAdmin) {
            clientConfig.register(HttpAuthenticationFeature.basic("admin", "setup"));
        }

        WebTarget webTarget = ClientBuilder.newClient(clientConfig)
                .target(endpoint)
                .path(path);
        if (queryParams != null) {
            queryParams.forEach(webTarget::queryParam);
        }
        return webTarget.request(MediaType.APPLICATION_JSON);
    }

}
