package ru.nsu.fit.tests.gui;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.gui.screen.CreateCustomerScreen;
import ru.nsu.fit.tests.gui.screen.CustomersScreen;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CreateCustomerEmptyPasswordTest {
    private BrowserService browser;
    private CustomersScreen customersScreen;
    private CreateCustomerScreen createCustomerScreen;

    @BeforeClass
    public void beforeClass() {
        browser = new BrowserService();
        browser.openCustomersPage();
        customersScreen = browser.openCustomersPage();
    }

    @Test
    @Title("Create customer with empty password")
    @Severity(SeverityLevel.CRITICAL)
    public void testCreateCustomerEmptyPassword() {
        createCustomerScreen = customersScreen.clickCreateCustomer();
        createCustomerScreen.enterFirstName("Elon");
        createCustomerScreen.enterLastName("Musk");
        createCustomerScreen.enterEmail("mail@mail.mail");
        createCustomerScreen.enterPassword("");
        createCustomerScreen.clickCreateCustomer();
        Assert.assertTrue(browser.isAlertPresent());
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
