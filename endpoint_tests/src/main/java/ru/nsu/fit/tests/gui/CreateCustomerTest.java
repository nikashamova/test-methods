package ru.nsu.fit.tests.gui;

import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.gui.screen.CreateCustomerScreen;
import ru.nsu.fit.tests.gui.screen.CustomersScreen;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CreateCustomerTest {
    private BrowserService browser;
    private String firstNameWithUniqueSuffix;
    private CustomersScreen customersScreen;
    private CreateCustomerScreen createCustomerScreen;

    @BeforeClass
    public void beforeClass() {
        browser = new BrowserService();
        customersScreen = browser.openCustomersPage();
        firstNameWithUniqueSuffix = String.format("Elon%s", RandomStringUtils.randomAlphabetic(4).toLowerCase());
    }

    @Test
    @Title("Create customer")
    @Severity(SeverityLevel.CRITICAL)
    public void testCreateCustomer() {
        createCustomerScreen = customersScreen.clickCreateCustomer();
        createCustomerScreen.enterFirstName(firstNameWithUniqueSuffix);
        createCustomerScreen.enterLastName("Musk");
        createCustomerScreen.enterEmail("tesla@boring.com");
        createCustomerScreen.enterPassword("spacex@boring.com");
        createCustomerScreen.clickCreateCustomer();
        customersScreen.enterQuery(firstNameWithUniqueSuffix);
        Assert.assertEquals(customersScreen.getFirstRowText(1), firstNameWithUniqueSuffix);
        Assert.assertEquals(customersScreen.getFirstRowText(2), "Musk");
        Assert.assertEquals(customersScreen.getFirstRowText(3), "tesla@boring.com");
        Assert.assertEquals(customersScreen.getFirstRowText(4), "spacex");
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
