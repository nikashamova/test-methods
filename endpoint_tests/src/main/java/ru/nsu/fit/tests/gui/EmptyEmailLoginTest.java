package ru.nsu.fit.tests.gui;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.gui.screen.LoginScreen;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class EmptyEmailLoginTest {
    private BrowserService browser;
    private LoginScreen loginScreen;

    @BeforeClass
    public void beforeClass() {
        browser = new BrowserService();
        loginScreen = browser.openLoginPage();
    }


    @Test
    @Title("Login with empty email")
    @Severity(SeverityLevel.CRITICAL)
    public void testEmptyEmailLogin() {
        loginScreen.enterEmail("email");
        loginScreen.enterPassword("");
        loginScreen.clickLogin();
        Assert.assertTrue(browser.isAlertPresent());
    }


    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
