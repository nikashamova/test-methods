package ru.nsu.fit.tests.gui;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.tests.gui.screen.LoginScreen;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class SuccessfulLoginTest {
    private LoginScreen loginScreen;
    private BrowserService browser;

    @BeforeClass
    public void beforeClass() {
        browser = new BrowserService();
        loginScreen = browser.openLoginPage();
    }

    @Test
    @Title("Login test")
    @Severity(SeverityLevel.CRITICAL)
    public void testSuccessfulLogin() {
        loginScreen.enterEmail("admin");
        loginScreen.enterPassword("setup");
        loginScreen.clickLogin();
    }


    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
