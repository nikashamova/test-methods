package ru.nsu.fit.tests.gui.screen;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.nsu.fit.services.browser.BrowserService;

public class CreateCustomerScreen {
    private static final String firstNameId = "first_name_id";
    private static final String lastNameId = "last_name_id";
    private static final String emailId = "email_id";
    private static final String passwordId = "password_id";
    private static final String createCustomerId = "create_customer_id";

    private BrowserService browser;

    @FindBy(id = firstNameId)
    private WebElement firstName;

    @FindBy(id = lastNameId)
    private WebElement lastName;

    @FindBy(id = emailId)
    private WebElement email;

    @FindBy(id = passwordId)
    private WebElement password;

    @FindBy(id = createCustomerId)
    private WebElement createCustomer;

    public CreateCustomerScreen(BrowserService browser) {
        this.browser = browser;
        PageFactory.initElements(browser.getWebDriver(), this);
        browser.makeScreenshot();
    }

    public void enterEmail(String email) {
        browser.sendKeys(this.email, email);
    }

    public void enterPassword(String password) {
        browser.sendKeys(this.password, password);
    }

    public void enterFirstName(String firstName) {
        browser.sendKeys(this.firstName, firstName);
    }

    public void enterLastName(String lastName) {
        browser.sendKeys(this.lastName, lastName);
    }

    public CustomersScreen clickCreateCustomer() {
        browser.click(createCustomer);
        return new CustomersScreen(browser);
    }

}
