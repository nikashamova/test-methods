package ru.nsu.fit.tests.gui.screen;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.nsu.fit.services.browser.BrowserService;

public class CustomersScreen {
    private static final String addNewCustomerId = "add_new_customer";
    private static final String inputPath = "(//div//input)";

    @FindBy(id = addNewCustomerId)
    private WebElement createCustomer;

    @FindBy(xpath = inputPath)
    private WebElement input;

    private BrowserService browser;

    public CustomersScreen(BrowserService browser) {
        this.browser = browser;
        PageFactory.initElements(browser.getWebDriver(), this);
        browser.makeScreenshot();
    }

    public CreateCustomerScreen clickCreateCustomer() {
        browser.click(createCustomer);
        return new CreateCustomerScreen(browser);
    }

    public void enterQuery(String query) {
        browser.sendKeys(this.input, query);
    }

    public String getFirstRowText(int column) {
        return browser.getFirstRowText(column);
    }
}
