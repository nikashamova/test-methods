package ru.nsu.fit.tests.gui.screen;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.nsu.fit.services.browser.BrowserService;

public class LoginScreen {
    private static final String emailId = "email";
    private static final String passwordId = "password";
    private static final String loginId = "login";

    private BrowserService browser;

    @FindBy(id = emailId)
    private WebElement email;

    @FindBy(id = passwordId)
    private WebElement password;

    @FindBy(id = loginId)
    private WebElement login;

    public LoginScreen(BrowserService browser) {
        this.browser = browser;
        PageFactory.initElements(browser.getWebDriver(), this);
        browser.makeScreenshot();
    }

    public void enterEmail(String email) {
        browser.sendKeys(this.email, email);
    }

    public void enterPassword(String password) {
        browser.sendKeys(this.password, password);
    }

    public CustomersScreen clickLogin() {
        browser.click(login);
        return new CustomersScreen(browser);
    }
}
